# MiscellaneousDocumentation

Various how-to, description, and other documents

## Documentation
* [WindowsAudioRerouting](WindowsAudioRerouting.md)
* [TMNF_user_quickstart](TMNF_user_quickstart.md)
* [Linux VRChat Setup](VRCLinuxSetup.md)

## License

Unless otherwise noted, all documentation (excluding trademarks or other 3rd-party properties) is dedicated to the public domain.
