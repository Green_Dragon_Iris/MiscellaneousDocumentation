# Windows Audio Rerouting
These instructions are for users who want to share audio between a Windows machine and other machines.

[[_TOC_]]

## Basic setup
1. Install the required programs.
    * [ASIO4All](https://www.asio4all.org/)
    * [JACK](https://jackaudio.org/downloads/)
    * [Synchronous Audio Router](https://github.com/eiz/SynchronousAudioRouter/releases/latest)
    * [asioconfig](https://github.com/jprjr/asioconfig/releases/latest)
    * [Mumble](https://www.mumble.info/downloads/)
        * Install the client and also the server.
        * This can be any audio program. I am using this one simply for low latency, high quality, and ease of setup.
1. Reboot. If you know how to perform this better, please send an MR. Maybe, disabling and reenabling the audio card would work?
1. Run QjackCtl and click "Start".
1. Run Mumble (or other audio call software) and connect to your local server.

## Outbound
You have audio on a Windows machine and want to hear it on another machine.

### Interface Creation

1. Run asioconfig.
1. Double-click Synchronous Audio Router.
1. Under Hardware Interface, select ASIO4ALL.
1. Add two entries.
    1. Fake Input
        * Name: OutboundPlayback
        * Type: Playback
    1. Fake Output
        * Name: OutboundRecording
        * Type: Recording

### Interface Configuration

1. Run QjackCtl.
1. Open Patchbay.
1. If you already have a saved config, go to the next step.
    1. Make a new config (New button on the top-left).
    1. Save this config somewhere you want to keep it.
    1. Activate the config (button on the top-right).
        * It will say "[Active]" in the drop-down box.
1. Click "Add" on the left column.
1. Name the socket "OutboundPlayback"
1. Under Client, select "system".
1. Under Plug, select the fake output channels you just made (probably the highest numbers) and click Add Plug.
    * If you added the default 2-channel (stereo) interface, this will be the highest-numbered 2 plugs.
1. Click OK.
1. Click "Add" on the right column, and perform the same steps as you did for the left column, naming it "OutboundRecording".
1. Select the new socket for each column, and at the bottom of the window, click "Connect".
1. Save the configuration.
1. Set Windows' default playback device to OutboundPlayback.

### Audio Call Configuration

1. In Mumble settings, set your Audio Input Device to OutboundRecording.

## Inbound
You have audio on another machine and want to hear it on Windows.

### Interface Creation

1. Run asioconfig.
1. Double-click Synchronous Audio Router.
1. Under Hardware Interface, select ASIO4ALL.
1. Add two entries.
    1. Fake Input
        * Name: InboundPlayback
        * Type: Playback
    1. Fake Output
        * Name: InboundRecording
        * Type: Recording

### Interface Configuration

1. Run QjackCtl.
1. Open Patchbay.
1. If you already have a saved config, go to the next step.
    1. Make a new config (New button on the top-left).
    1. Save this config somewhere you want to keep it.
    1. Activate the config (button on the top-right).
        * It will say "[Active]" in the drop-down box.
1. Click "Add" on the left column.
1. Name the socket "InboundPlayback"
1. Under Client, select "system".
1. Under Plug, select the fake output channels you just made (probably the highest numbers) and click Add Plug.
    * If you added the default 2-channel (stereo) interface, this will be the highest-numbered 2 plugs.
1. Click OK.
1. Click "Add" on the right column, and perform the same steps as you did for the left column, naming it "InboundRecording".
1. Select the new socket for each column, and at the bottom of the window, click "Connect".
1. Save the configuration.
1. Set Windows' default recording device to InboundRecording.

### Audio Call Configuration

1. In Mumble settings, set your Audio Output Device to InboundPlayback.
