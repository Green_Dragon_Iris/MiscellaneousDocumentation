This guide will walk you through downloading and connecting to a dedicated server. This is because of a limitation intentionally added by Nadeo to TMNF (free) trying to make more people buy TrackMania United Forever (paid). It is also because they let the game go unupdated for years.

# Setup

## Install
1. Download TMNF.
    - [Steam](https://store.steampowered.com/app/11020/TrackMania_Nations_Forever/?snr=1_7_7_151_150_1)
    - [Directly from the site](https://nadeo-download.cdn.ubi.com/trackmaniaforever/tmnationsforever_setup.exe)


## Fix the game
1. For Windows 7 and newer, read below. Linux users may skip to the next section.
1. Edit your nadeo.ini file. Path is given for the Steam version.
    - C:\Program Files\(x86)\Steam\steamapps\common\TrackMania Nations Forever\Nadeo.ini
    1. Change the line that has `Distro=` on it and make it `Distro=MILIN`.
    1. Save the file and close the editor.
1. Download and run [the patch](http://files2.trackmaniaforever.com/TmUnitedForever_Update_2010-03-15_Setup.exe).
1. Launch the game settings.
    - Launch the game and select the settings option.
1. Go into the configuration and set everything how you like it.
    - Modern GPUs may simply be misclassed and set to very low settings.
1. Save the settings.

## Create an account
1. Launch the game.
1. Pick a username and passphrase.
    - max 8 characters
    - letters and numbers only
    - It is recommended you put in an email in case of a lost passphrase.

## Profile Setup
1. Launch and log into the game.
1. On the left side, select "Profile".
1. Use the top-middle box under "Nickname" to change your Nickname.
1. To change your profile picture, click the picture in the top Profile section.
    - .jpg format
    - max 16KB
    - max 128x128
1. To change your horn sound, Click the speaker picture in the Profile section.
    - You can select one of the built-in horns.
    - You can also use a custom file.
        - .wav format
        - mono
        - max 10 seconds
        - in your horn folder: `%USERPROFILE%\My Documents\TrackMania\Skins\Horns`
1. To change your vehicle skin, select "Vehicles" on the left side.
    - You can select one of the prebuilt ones.
    - You can also paint one with the "Paint!" button.
        - Remember to select it after you do.
    - You can also download one and put it into your folder: `%USERPROFILE%\My Documents\TrackMania\Skins\Vehicles`.
1. To change your controls, select "Input" on the left side.
    - There is only digital (on/off) gas/brakes. The "analog" options are broken.
    - Analog steering still works.

# Playing the Game
- You can play single-player to race through the tracks alone.
- You can also play on Freezone servers (as long as they still exist).
- You can also play on dedicated servers with your friends.
    1. Mouse over the top of the window to get the drop-down bar to appear.
    1. On the left of the bar is a purple sphere in a blue cube. Click it for the browser.
    1. Replace `serverlogin` with the server's login in the upcoming 2 commands then paste them into the browser's URL bar and hit your [enter] key.
        - `tmtp://#addfavourite=serverlogin`
            - You only need to do this one time per server, but you must do it.
        - `tmtp://#join=serverlogin`
            - This will actually connect you to that server.

# Resources
- https://github.com/EvoTM/awesome-trackmania#Servers
- https://gamebanana.com/tuts/11236
- https://www.pcgamingwiki.com/wiki/TrackMania_Nations_Forever
- http://www.tm-forum.com/viewtopic.php?t=14203
