# VRCLinuxSetup
Run the script [VRCLinuxSetup.sh](VRCLinuxSetup.sh), and follow all the prompts.

Keep in mind, the Linux version of the mod manager does not yet handle mod dependencies. You may need to watch the terminal that now launches with VRC to see what is missing.
