#!/usr/bin/env bash
#Original instructions here: https://github.com/RinLovesYou/VRCLinuxAssistant/wiki/Prerequisites-for-installing-MelonLoader-under-Wine-Proton

#If you can, install Protontricks through your package manager.
# Otherwise, see: https://github.com/Matoking/protontricks#installation
VRCSteamID=438100
VRCLinuxAssistantURL='https://github.com/RinLovesYou/VRCLinuxAssistant/releases/latest/download/VRCLinuxAssistant'
DLDir="$HOME/Downloads"
VRCPrefix=`protontricks -c 'cd "$WINEPREFIX" && cd .. && pwd' $VRCSteamID`

mv "$VRCPrefix" "$VRCPrefix.old"
read -p "Set Proton version for VRC to 4.11-13. Hit [Enter], and then close VRChat when it opens."
steam steam://run/$VRCSteamID
protontricks $VRCSteamID --force vcrun2019
protontricks $VRCSteamID --force dotnet472
sleep 3
echo ""
read -p "Set Proton version for VRC to the latest/experimental version, hit [Enter], switch Windows version to Windows 10, go to the Libraries tab, add an override for 'version', hit apply, and then hit OK."
protontricks $VRCSteamID winecfg

#Time for mods!
mkdir -p "$DLDir"
pushd "$DLDir"
wget "$VRCLinuxAssistantURL"
chmod u+x VRCLinuxAssistant
VRCDirectory=`protontricks -c 'echo "$PWD"' $VRCSteamID`
read -p "Hit [enter] and use the path: '$VRCDirectory'."
./VRCLinuxAssistant
popd