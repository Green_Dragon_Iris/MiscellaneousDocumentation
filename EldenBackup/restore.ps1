#!/usr/bin/env powershell
. "$PSScriptRoot\env.ps1"

copy-item -recurse -force $BackupLocation\* "$SourceLocation\"
