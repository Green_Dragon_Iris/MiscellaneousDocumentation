#!/usr/bin/env powershell
. "$PSScriptRoot\env.ps1"

New-Item -ItemType Directory -Force -Path "$BackupLocation"
copy-item -recurse -Force $SourceLocation\* "$BackupLocation\"
