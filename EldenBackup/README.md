# README
If you want these files to work, keep them all together.

## Configure
If you want to change your backup location or need to set your custom appdata location, edit the relevant variable in `env.ps1`. If you are confused by what this means, the scripts should work without issue.

## Backup
Run backup.ps1.

## Restore
Run restore.ps1

## Help running a powershell script
If you are struggling to run a powershell script, try the instructions below. Do the simple method at least once so you can have powershell trust the script.

### Simple method
right-click -> Run with PowerShell

### Set Windows up to run it with double-click
1. right-click -> Properties
1. Opens with: -> (Change...)
1. More apps
1. Scroll to the end.
1. Look for another app on this PC.
1. In the "File name:" field, paste in your powershell location (default "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe").
1. (Open)
1. (OK)

You can now double-click a powershell script to have it run.
